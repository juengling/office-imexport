Attribute VB_Name = "ImExCode"
''
' Import and export the code of this project
' Needs reference "Microsoft Visual Basic for Appplications Extensibility 5.3"
' for early binding.
'
' @author   Christoph J�ngling <Chris@Juengling-EDV.de>
'
Option Explicit

Private Const CLASS_NAME = "ImExCode"

''
' Export code of current project
' Needs reference "Microsoft Visual Basic for Appplications Extensibility 5.3"
' for early binding.
'
' @author   Christoph J�ngling
'
Public Sub DoExport()

Const FUNCTION_NAME = "DoExport"

Dim activedoc As Object
Dim VBComp As Object ' VBIDE.VBComponent
Dim Path As String
Dim ext As String

'-----------------------

On Error GoTo Catch

Set activedoc = GetActiveDoc()
Path = AddBackslash(activedoc.Path) & activedoc.name & ".src\"
CreateFolderHierarchy Path

Debug.Print "Exporting to " & Path

For Each VBComp In activedoc.VBProject.VBComponents
    If VBComp.Type = 100 Then
        ext = ".cls"
    Else
        ext = Choose(VBComp.Type, ".bas", ".cls", ".frm")
    End If
    
    Debug.Print "Export " & VBComp.name & ext
    VBComp.Export Path & VBComp.name & ext
Next VBComp

Debug.Print "Done."

'-----------------------
Final:
Exit Sub

'-----------------------
Catch:
Select Case Err.Number
    Case 1004
        MsgBox Err.Description, vbCritical, FUNCTION_NAME
        
    Case Else
        Debug.Print Err.Number, Err.Source, Err.Description
End Select

Resume Final
Resume

End Sub

''
' Export code of current project
'
' @author   Christoph J�ngling
'
Public Sub DoImport()

Const FUNCTION_NAME = "DoImport"

Dim activedoc As Object
Dim extensions As Variant
Dim i As Integer
Dim Path As String
Dim filename As String
Dim compname As String

'-----------------------

On Error GoTo Catch

Set activedoc = GetActiveDoc()
Path = AddBackslash(activedoc.Path) & activedoc.name & ".src\"
extensions = Array("bas", "cls", "frm")

Debug.Print "Importing from " & Path

For i = LBound(extensions) To UBound(extensions)
    filename = Dir(Path & "*." & extensions(i))
    Do While filename <> ""
        With activedoc.VBProject.VBComponents
            compname = Split(filename, ".")(0)
            If compname = "ThisDocument" Then
                Debug.Print "Import of " & filename & " skipped"
            Else
                Debug.Print "Import " & filename
                .Item(compname).name = compname & "_OLD"
                .Import Path & filename
                .Remove .Item(compname & "_OLD")
            End If
        End With
        
        filename = Dir()
    Loop
Next i

Debug.Print "Done."

'-----------------------
Final:
Exit Sub

'-----------------------
Catch:
Select Case Err.Number
    Case 1004
        MsgBox Err.Description, vbCritical, FUNCTION_NAME
        
    Case Else
        Debug.Print Err.Number, Err.Source, Err.Description
End Select

Resume Final
Resume

End Sub

''
' Get instance for the current application's main document object
'
' @return   Object
' @author   Christoph J�ngling
'
Private Function GetActiveDoc() As Object

' This code works fine for Office 2010 and probably 2013.

' In Office 2016 Microsoft changed the behaviour of the underlaying compiler.
' From now on you'll get sytnax errors, hence you must deactivate any of these
' lines you don't need for the particular application!

On Error Resume Next
Set GetActiveDoc = Application.ActiveDocument ' Word, Visio
'Set GetActiveDoc = Application.ActiveWorkbook ' Excel
'Set GetActiveDoc = Application.ActivePresentation ' Powerpoint

End Function

''
' Create folder hierarchy if not existent
'
' @param    Path (String)  Intended Path
' @author   Christoph J�ngling
'
Private Sub CreateFolderHierarchy(ByVal Path As String)

Dim levels As Variant
Dim i As Integer
Dim j As Integer
Dim testpath As String

'---------------------

levels = Split(Path, "\")
For i = LBound(levels) To UBound(levels)
    testpath = ""
    For j = LBound(levels) To i
        testpath = testpath & levels(j) & "\"
    Next j
    
    If Dir(testpath, vbDirectory) = vbNullString Then
        MkDir testpath
    End If
Next i

End Sub

''
' Add trailing backslash, if it's not already there
'
' @param    source (String)  Original Path
' @return   Same Path as original, but potentially with an additional backslash at the end
' @author   Christoph J�ngling
'
Private Function AddBackslash(ByVal Source As String) As String

If Right(Source, 1) <> "\" Then
    AddBackslash = Source & "\"
Else
    AddBackslash = Source
End If

End Function
