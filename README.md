# Office Im- & Export

This code helps to export all VBA code into files. There is not much
convenience, and it is no Addin/Plugin. Just code to export code.
Simple enough.

# Installation

## Import the code

All the code you need is combined in one module named `ImExCode`.
So first import this file from the appropriate folder into your project.

- `Test-Calculation.xlsm.src` for Excel
- `Test-Document.docm.src` for Word
- `Test-Drawing.vsdm.src` for Visio
- `Test-Presentation.pptm.src` for Powerpoint

Other Office programs may work with similar code, try it out!

**For Microsoft Access** I recommend to have a close look at the projects
[OASIS](https://dev2dev.de) and [Ivercy](https://ivercy.com), which do much more
than the here provided code can do.


## Enable code access

Allow the VBA code to access itself. So go to
**FILE / Options / Trust Center / Settings for Trust Center / Macro settings**
and check the "allow access to the VBA project model" (or however it may be
called in your Office version and language). Save all dialogs with OK, exit
the program and restart it again.

This may be a security issue, so please **do not** do this an every machine
you come around. Because a user doesn't need to export the VBA code, s/he
doesn't need this permission to access the code itself.
 

## Make some tiny changes in code

Go to the `GetActiveDoc` subroutine. You may see code like this:

	On Error Resume Next
	Set GetActiveDoc = Application.ActiveDocument ' Word, Visio
	Set GetActiveDoc = Application.ActiveWorkbook ' Excel
	Set GetActiveDoc = Application.ActivePresentation ' Powerpoint

This code works fine for Office 2010 and probably 2013.

In Office 2016 Microsoft changed the behaviour of the underlaying compiler.
From now on you'll get sytnax errors, which cannot be suppressed by the
`On Error Resume Next`. Hence you must deactivate any of these
lines you don't need for the particular application!

Save your document "with macros", i.e. with an "m" instead of the "x" at the end of
the file type. This is probably already done, because otherwise you can't
store your own macros in it.


# Execution

Export the code by running `DoExport` and have a look at the file system.
You shall find a new folder with the same name as the document and an additional
`.src` at its end. Here you'll find all files exported from the code - including
the export code module itself.

Add everything to your source code control tool. Please add your main document /
workbook / presentation as well, because the above code only exports VBA code,
but nothing else (at the moment).

If you want to get rid of unmotivated case changes, a close look at my other project
[Casey](https://gitlab.com/juengling/casey) may be helpful.


# Import

There is a `DoImport` subroutine, but I'm not sure if it is working well under all
circumstances. Please be careful if you use this!

Any feedback is greatly appreciated. Feel free to file an issue in the
[GitLab project](https://gitlab.com/juengling/office-imexport/issues).